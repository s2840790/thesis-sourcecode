import random
import time
from main import *

S_N = create_S_N(N)
S = create_snapshot_matrix(NUM_PARAMATER_SAMPLES, True)
def plot_alg1_vs_alg2():
    to_test = list(range(1,N+1,1+N//10))
    if to_test[-1] != N:
        to_test.append(N)

    alg2 = [kolmogorov_n_width_no_sampling(n, S, S_N) for n in to_test]
    plt.plot(to_test, alg2, label=r'$d_n(S_N,V_N)$: Algorithm 2')

    for i in range(2,5):
        num_basis_samples = 10**i
        alg1 = [kolmogorov_n_width(n, num_basis_samples) for n in to_test]
        plt.plot(to_test, alg1, label= rf'$d_n(S_N, V_N)$: Algorithm 1, $N_s=10^{i}$')
        print(num_basis_samples)

    plt.xlabel(r"Reduced dimension $n$")
    plt.ylabel(r"Approximations of $d_n(S_N, V_N)$")
    show_plot(True, False, False)

def plot_alg3_vs_alg4():
    to_test = list(range(1,N+1,1+N//20))
    if to_test[-1] != N:
        to_test.append(N)

    alg2 = [poly_kolmogorov_n2_width_no_sampling(n, False, S, S_N, regularization_parameter) for n in to_test]
    plt.plot(to_test, alg2, label=r'$d_{n,2}^\otimes (S_N,V_N)$: Algorithm 4')

    for i in range(2,5):
        num_basis_samples = 10**i
        alg1 = [poly_kolmogorov_n2_width(n,num_basis_samples) for n in to_test]
        plt.plot(to_test, alg1, label= "$d_{n,2}^\otimes (S_N, V_N)$: Algorithm 3," +  f"$N_s=10^{i}$")
        print(num_basis_samples)

    plt.xlabel(r"Reduced dimension $n$")
    plt.ylabel(r"Approximations of $d_{n,2}^\otimes (S_N, V_N)$")
    show_plot(True, False, False)

def plot_ratio_alg1_alg2():
    to_test = list(range(1,N+1,1+N//20))
    if to_test[-1] != N:
        to_test.append(N)
    num_basis_samples = 10**5


    alg1 = np.array([kolmogorov_n_width(n, num_basis_samples) for n in to_test])
    alg2 = np.array([kolmogorov_n_width_no_sampling(n, S, S_N) for n in to_test])
    plt.plot(to_test, alg1/alg2)

    plt.xlabel(r"Reduced dimension $n$")
    plt.ylabel(r"$\rho$")
    show_plot(True, False, False)

def plot_ratio_algo2_algo4():
    to_test = list(range(1,N,2))

    alg4 = np.array([poly_kolmogorov_n2_width_no_sampling(n, False, S, S_N, regularization_parameter) for n in to_test])
    alg2 = np.array([kolmogorov_n_width_no_sampling(n, S, S_N) for n in to_test])
    plt.plot(to_test, alg2/alg4)

    plt.xlabel(r"Reduced dimension $n$")
    plt.ylabel(r"$\rho$")
    show_plot(True, False, False)

def plot_comparison_approximation_bounds():
    to_test = list(range(1,N+1,1+N//20))
    if to_test[-1] != N:
        to_test.append(N)
    n_widths_no_sampling = []
    t_n2_widths_no_sampling = []
    poly_n2_widths_no_sampling = []
    print(f"testing until n = {to_test[-1]}")
    start_time = time.time()
    times = []
    for n in to_test:
        n_widths_no_sampling.append(kolmogorov_n_width_no_sampling(n, S, S_N))
        t_n2_widths_no_sampling.append(kolmogorov_n_width_no_sampling(min(N, t(n, 2)), S, S_N))
        poly_n2_widths_no_sampling.append(poly_kolmogorov_n2_width_no_sampling(n, True, S, S_N, 20))
        curr_time = time.time() - start_time
        print(f"n={n}, time={round(curr_time,2)}")
        times.append(curr_time)
    print(f"total time taken={round(time.time() - start_time, 2)}")
    print(np.average(n_widths_no_sampling) / np.average(poly_n2_widths_no_sampling))
    plt.plot(to_test, t_n2_widths_no_sampling, label=r'$d_{t(n,2)}(S_N,V_N)$: Algorithm 2')
    plt.plot(to_test, n_widths_no_sampling, label=r'$d_n(S_N,V_N)$: Algorithm 2')
    plt.plot(to_test, np.array(poly_n2_widths_no_sampling), label=r'$d_{n,2}^\otimes (S_N,V_N)$: Algorithm 4)')
    plt.xlabel(r"Reduced dimension $n$")
    plt.ylabel(r"Approximations of widths")
    #save_plot(plt_name)
    show_plot(True, False, True)

def plot_box(cs, label, smooth=False):
    xs = np.arange(0,1,1/N)
    x_points = np.repeat(xs, 2)[1:]
    x_points = np.append(x_points, xs[-1] + 1)

    ys = []
    for c in cs:
        ys.append(c*np.sqrt(N))

    y_points = np.repeat(ys, 2)
    plt.plot(x_points,y_points, label=label)
    if smooth:
        plt.plot(xs, ys, label='smooth version')


def plot_box_reconstruction():
    x = random.choice(S_N)
    plot_box(x, rf"box function of dimension $N={N}$")


    for n in [24]:
        V1 = lalg.orth(S)[:,:n]
        linear_approximation = np.dot(V1@V1.T,x)
        plot_box(linear_approximation, rf'Algorithm 2 reconstruction with $n={n}$')

        Ns = 10**2
        Vr = arg_kolmogorov_n_width(n, Ns)
        random_linear_approximation = np.dot(Vr@Vr.T,x)
        plot_box(random_linear_approximation, rf'Algorithm 1 reconstruction with $N_s=10^5$ and $n={n}$')

    plt.xlim(0, 1)
    show_plot(True, False, False)

def get_worst_x(basis):
    I_N = np.eye(N)
    worst_x = None
    worst_error = 0
    for x in S_N:

        temp = np.dot(I_N - basis@basis.T,x)
        best_approximation_error = np.dot(temp,temp) ** 0.5
        if best_approximation_error > worst_error:
            worst_error = best_approximation_error
            worst_x = x

    return worst_x, worst_error
def find_worst_sup_approximation(n):
    V1 = lalg.orth(S)[:,:n]

    Ns = 10**3
    Vr = arg_kolmogorov_n_width(n, Ns)


    worst_alg1,worst_alg1_error = get_worst_x(Vr)
    worst_alg2,worst_alg2_error = get_worst_x(V1)

    worst_alg1_approximation = np.dot(Vr@Vr.T, worst_alg1)
    worst_alg2_approximation = np.dot(V1@V1.T, worst_alg2)
    print(f"worst_alg1: {worst_alg1_error}, worst_alg2: {worst_alg2_error}")

    plot_box(worst_alg1, rf"box function of dimension $N={N}$")
    plot_box(worst_alg1_approximation, rf"Algorithm 1 reconstruction with $n={n}$")
    show_plot(True)
    # plot_box(worst_alg2, rf"box function of dimension $N={N}$")
    # plot_box(worst_alg2_approximation, rf"Algorithm 2 reconstruction with $n={n}$")
    # show_plot(True)
    # show_plot(True)

def plot_sref_vs_nosref():
    to_test = list(range(1,N+1,1+N//10))
    if to_test[-1] != N:
        to_test.append(N)


    nosref = np.array([poly_kolmogorov_n2_width_no_sampling(n, False, S, S_N, regularization_parameter) for n in to_test])
    sref = np.array([poly_kolmogorov_n2_width_no_sampling(n, True, S, S_N, regularization_parameter) for n in to_test])
    plt.plot(to_test, sref, label='sref')
    plt.plot(to_test, nosref, label= 'nosref')
    plt.xlabel(r"Reduced dimension $n$")
    plt.ylabel(r"$\rho$")
    show_plot(True, False, False)


def temeprature_idea(V1, n):
    def create_S_N_2(N):
        as_ = np.arange(0,1,1/N)
        ls = np.arange(lmin, 1-lmin,1/N)
        cs = []
        params = []
        for a in as_:
            for l in ls:
                cs.append(create_coefficients(a,l))
                params.append((a,l))
        return np.array(cs), params

    cs, params = create_S_N_2(N)
    print("worst best-approximation error: " + str(get_worst_x(V1)[1]))
    I_N = np.eye(N)
    a_values = [param[0] for param in params]
    l_values = [param[1] for param in params]

    temperatures = []
    for c in cs:
        temp = np.dot(I_N - V1@V1.T,c)
        error = np.dot(temp,temp) ** 0.5
        temperatures.append(error)

    print("Average error: " + str(np.average(temperatures)))
    a_unique = np.unique(a_values)
    l_unique = np.unique(l_values)
    temperature_grid = np.full((len(l_unique), len(a_unique)), np.nan)
    for (a, l), temp in zip(params, temperatures):
        a_idx = np.where(a_unique == a)[0][0]
        l_idx = np.where(l_unique == l)[0][0]
        temperature_grid[l_idx, a_idx] = temp

    plt.figure(figsize=(10, 6))
    heatmap = plt.imshow(temperature_grid, cmap='viridis', origin='lower', extent=[min(a_unique), max(a_unique), min(l_unique),max(l_unique)])

    # Add color bar which maps values to colors
    colorbar = plt.colorbar(heatmap)
    colorbar.set_label('Approximation error')

    # Labels
    plt.xlabel('a')
    plt.ylabel('l')
    plt.title(rf'Approximation error for box functions $b_{{a,l}}$ with n={n} and N={N}')

    # Show plot
    show_plot(False, False, False)

def plot_comparison_approximation_bounds2():
    to_test = list(range(1,N+1,1+N//20))
    if to_test[-1] != N:
        to_test.append(N)
    n_widths_no_sampling = []
    t_n2_widths_no_sampling = []
    poly_n2_widths_no_sampling = []
    print(f"testing until n = {to_test[-1]}")
    start_time = time.time()
    times = []
    num_basis_samples = 10**3
    for n in to_test:
        n_widths_no_sampling.append(kolmogorov_n_width(n, num_basis_samples))
        t_n2_widths_no_sampling.append(kolmogorov_n_width(min(N, t(n, 2)), num_basis_samples))
        poly_n2_widths_no_sampling.append(poly_kolmogorov_n2_width(n, num_basis_samples))
        curr_time = time.time() - start_time
        print(f"n={n}, time={round(curr_time,2)}")
        times.append(curr_time)
    print(f"total time taken={round(time.time() - start_time, 2)}")
    print(np.average(n_widths_no_sampling) / np.average(poly_n2_widths_no_sampling))
    plt.plot(to_test, t_n2_widths_no_sampling, label=r'$d_{t(n,2)}(S_N,V_N)$: Algorithm 1')
    plt.plot(to_test, n_widths_no_sampling, label=r'$d_n(S_N,V_N)$: Algorithm 1')
    plt.plot(to_test, np.array(poly_n2_widths_no_sampling), label=r'$d_{n,2}^\otimes (S_N,V_N)$: Algorithm 3)')
    plt.xlabel(r"Reduced dimension $n$")
    plt.ylabel(r"Approximations of widths")
    show_plot(True, False, True)

n = N - 1
V1 = lalg.orth(S)[:,:n]
temeprature_idea(V1, n)
