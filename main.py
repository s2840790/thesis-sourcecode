import itertools

import numpy as np
import scipy.linalg as lalg

import math

import matplotlib.pyplot as plt
import seaborn as sns
import mpld3

sns.set_style('whitegrid')
sns.set_context('paper', font_scale=1)



b = 1
N = 25
lmin = 1/N # has to multiple of 1/N
NUM_BASIS_SAMPLES = 2_00
NUM_PARAMATER_SAMPLES = 690 # not necessary anymore
regularization_parameter = 20#20#400*N
dpi = 200
figsize = (5,3)

def is_multiple(x, y, tol=1e-9):
    return abs(x % y) < tol

assert is_multiple(lmin, 1/N), "lmin is not a multiple of 1/N"

def show_plot(legend, scientific_x=True, scientific_y=True):
    if legend:
        leg = plt.legend()
        leg.set_draggable(True)
    plt.rcParams['text.usetex'] = True
    plt.rcParams['font.family'] = 'serif'

    fig = plt.gcf()
    fig.set_dpi(dpi)
    fig.set_size_inches(*figsize)
    if scientific_x:
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    if scientific_y:
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

    plt.show()

def sample_linear_and_quadratic_basis(N, n):
    linearBasis = sample_linear_basis(N,n)
    return linearBasis, sample_quadratic_basis(N, n, linearBasis)

def t(n, p):
    return sum(math.comb(n + k - 1, k) for k in range(p + 1))

def sample_linear_basis(N, n):
    return lalg.orth(np.random.rand(N,n))

def sample_quadratic_basis(N, n, linBasis):
    num_columns = t(n,2) - n - 1
    orth_random_matrix = lalg.orth(np.random.rand(N,num_columns))

    if orth_random_matrix.shape[1] < num_columns: # if the SVD truncated the random amtrix
        zeros_padding = np.zeros((N, num_columns - orth_random_matrix.shape[1]))
        orth_random_matrix = np.hstack((orth_random_matrix, zeros_padding))

    quadBasis = (np.eye(N) - linBasis @ linBasis.T) @ orth_random_matrix
    return quadBasis


def create_S_N(N):
    as_ = np.arange(0,1,1/N)
    ls = np.arange(lmin, 1-lmin,1/N)
    cs = []
    for a in as_:
        for l in ls:
            cs.append(create_coefficients(a,l))

    return np.array(cs)

def create_coefficients(a,l):
    c = []
    for x in np.arange(0,1,1/N):
        if a + l > 1:
            c.append(b/(N**0.5) if (x >= a or x < (a+l)%1) else 0)
        else:
            c.append(0 if x < a or x >= a + l else b/(N**0.5))
    return c


def kolmogorov_n_width(n, num_basis_samples, S_N = None):
    m_width = np.inf
    if S_N == None:
        S_N = create_S_N(N)
    I_N = np.eye(N)
    for temp in range(num_basis_samples):
        basis = sample_linear_basis(N, n)
        worst_best_approximation_error = 0
        for x in S_N:
            temp = np.dot(I_N - basis@basis.T, x)
            best_approximation_error = np.dot(temp,temp) ** 0.5
            worst_best_approximation_error = max(worst_best_approximation_error, best_approximation_error)
        m_width = min(m_width, worst_best_approximation_error)
    return m_width

def arg_kolmogorov_n_width(n, num_basis_samples):
    m_width = np.inf
    I_N = np.eye(N)
    best_basis = None
    S_N = create_S_N(N)
    for temp in range(num_basis_samples):
        basis = sample_linear_basis(N, n)
        worst_best_approximation_error = 0
        for x in S_N:
            temp = np.dot(I_N - basis@basis.T, x)
            best_approximation_error = np.dot(temp,temp) ** 0.5
            worst_best_approximation_error = max(worst_best_approximation_error, best_approximation_error)

        if worst_best_approximation_error < m_width:
            m_width = worst_best_approximation_error
            best_basis = basis
    return best_basis

def kolmogorov_n_width_no_sampling(n, S, S_N):
    m_width = 0
    I_N = np.eye(N)
    Vn = lalg.orth(S)[:, :n]

    for x in S_N:
        temp = np.dot(I_N - Vn@Vn.T, x)
        best_approximation_error = np.dot(temp,temp) ** 0.5
        m_width = max(m_width, best_approximation_error)

    return m_width
def symmetric_kronecker_product_order_2(x):
    unique_combinations = itertools.combinations_with_replacement(x,2)
    sym_kron2 = np.zeros(math.comb(len(x) + 2 - 1, 2))
    for i,term in enumerate(unique_combinations):
        sym_kron2[i] = math.prod([_ for _ in term])
    return sym_kron2

def poly_kolmogorov_n2_width(n,  num_basis_samples):
    n2_width = np.inf
    S_N = create_S_N(N)
    for _ in range(num_basis_samples):
        A1,A2 = sample_linear_and_quadratic_basis(N,n)

        worst_best_approximation_error = 0
        for x in S_N:
            x_n = np.dot(A1.T,x)
            kron_xn = symmetric_kronecker_product_order_2(x_n)
            x_tilde = np.dot(A1, x_n) + np.dot(A2, kron_xn)

            diff = x - x_tilde
            best_approximation_error = np.dot(diff, diff) ** 0.5
            worst_best_approximation_error = max(best_approximation_error, worst_best_approximation_error)
        n2_width = min(n2_width, worst_best_approximation_error)
    return n2_width

def poly_kolmogorov_n2_width_no_sampling(n, choose_sref, S, S_N, reg):
    A1 = lalg.orth(S)[:,:n]
    A2, s_ref = create_quadratic_pod_basis(A1, S, choose_sref, reg)


    n2_width = 0
    for x in S_N:
        x_n = np.dot(A1.T,x - s_ref)
        kron_xn = symmetric_kronecker_product_order_2(x_n)
        x_tilde = s_ref + np.dot(A1, x_n) + np.dot(A2, kron_xn)
        diff = x - x_tilde
        best_approximation_error = np.dot(diff, diff) ** 0.5
        n2_width = max(best_approximation_error, n2_width)
    return n2_width

def poly_kolmogorov_n2_width_sampleV2(n, S, S_N):
    A1 = lalg.orth(S)[:,:n]
    n2_width = np.infty
    for _ in range(NUM_BASIS_SAMPLES):
        worst_best = 0
        A2 = sample_quadratic_basis(N,n, A1)
        for x in S_N:
            x_n = np.dot(A1.T,x)
            kron_xn = symmetric_kronecker_product_order_2(x_n)

            x_tilde = np.dot(A1, x_n) + np.dot(A2, kron_xn)
            diff = x - x_tilde
            best_approximation_error = np.dot(diff, diff) ** 0.5
            worst_best = max(best_approximation_error, worst_best)
        n2_width = min(n2_width, worst_best)
    return n2_width


def create_snapshot_matrix(num_parameters, sample_all_paramaters):
    mu_samples = sample_mus(num_parameters, sample_all_paramaters)#
    num_parameters = mu_samples.shape[0]
    snapshot_matrix = np.zeros((N, num_parameters))


    for i in range(num_parameters):
        a,l = mu_samples[i]
        coeff = create_coefficients(a,l)
        snapshot_matrix[:,i] = coeff

    return snapshot_matrix


def sample_mus(num_samples, sample_all):
    all_a = np.arange(0,1,1/N)
    all_l = np.arange(lmin, 1-lmin,1/N)
    if sample_all:
        cartesian_product = np.array(list(itertools.product(all_a, all_l)))
        return cartesian_product

    a = np.random.choice(all_a, num_samples, replace=True)
    l = np.random.choice(all_l, num_samples, replace=True)

    return np.vstack((a,l)).T

def create_W(S_HAT):
    k = S_HAT.shape[1]
    W = []

    for i in range(k):
        si_hat = S_HAT[:, i]
        kron = symmetric_kronecker_product_order_2(si_hat)
        W.append(kron)

    W = np.stack(W, axis=1)
    return W


def create_quadratic_pod_basis(A1, P, choose_p_ref, regularization_parameter):
    if choose_p_ref:
        p_ref = np.mean(P, axis=1)
    else:
        p_ref = np.zeros(P.shape[0])
    P_REF = np.tile(p_ref.reshape(-1, 1), P.shape[1])
    P_SHIFTED = P - P_REF
    I = np.eye(P.shape[0]) #6,6
    ER = (I - (A1 @ A1.T)) @ P_SHIFTED

    P_HAT = A1.T @ P_SHIFTED
    W = create_W(P_HAT)

    WW_T = W @ W.T
    gamma_I = regularization_parameter * np.eye(WW_T.shape[0])
    A2 = ER @ W.T @ np.linalg.inv(WW_T + gamma_I)
    return A2, p_ref

def save_plot(plt_name):
    mpld3.save_html(plt.gcf(), plt_name)
    html_str = mpld3.fig_to_html(plt.gcf(), no_extras=False)
    with open(plt_name, 'w') as f:
        f.write(html_str)


def test_reg_paramater(P, S_N):
    avgs = []
    to_test = range(1,N, N//10)
    for reg in [10**i for i in range(0,8)]:
        poly_n2_widths_2_no_sampling = [poly_kolmogorov_n2_width_no_sampling(n, True, P, S_N, reg) for n in to_test]
        avg = np.average(poly_n2_widths_2_no_sampling)
        avgs.append((avg,reg))
        print(f"reg = {reg},avg={avg}")
        plt.plot(to_test, poly_n2_widths_2_no_sampling, label=rf'$d_n^\otimes (S_N,V_N)$, $\gamma={reg}$')
    print(min(avgs))
    show_plot(True)

