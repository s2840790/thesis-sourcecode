import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

import main
from main import test_number_of_basis, test_number_of_basis2
from sklearn.metrics import r2_score


def exponential_decay(x, a, b):
    return a * np.exp(-b * x)

def linear_decay(x, a, b):
    return a - b * x

def power_law_decay(x, a, b, c):
    return a * x**(-b) + c

def logarithmic_decay(x, a, b):
    return a - b * np.log(x)

def inverse_decay(x, a, b):
    return a / (x + b)


def fit(x, y):
    # Fit the models to the data

    #popt_exp, _ = curve_fit(exponential_decay, x, y, maxfev=10000)
    #popt_lin, _ = curve_fit(linear_decay, x, y, maxfev=10000)
    popt_pow, _ = curve_fit(power_law_decay, x, y,p0=[0.44, 0.044, 0.2], maxfev=10000)
    #popt_log, _ = curve_fit(logarithmic_decay, x, y,p0=[0.42,0.011] ,maxfev=10000)
    #popt_inv, _ = curve_fit(inverse_decay, x, y, maxfev=10000)

    # Generate y values based on the fitted models
    #y_fit_exp = exponential_decay(x, *popt_exp)
    #y_fit_lin = linear_decay(x, *popt_lin)
    y_fit_pow = power_law_decay(x, *popt_pow)
    xs = [500*i for i in range(1, x[-1]//500)]
    y_fit_pow = power_law_decay(xs, *popt_pow)
    #y_fit_log = logarithmic_decay(x, *popt_log)
    #y_fit_inv = inverse_decay(x, *popt_inv)
    #r2_pow = r2_score(y, y_fit_pow)
    #r2_log = r2_score(y, y_fit_log)

    # Plot the original data and the fitted models
    plt.scatter(x, y, color='red', s=2)

    #plt.plot(x, y_fit_exp, label='Exponential Decay', linestyle='--')
    #plt.plot(x, y_fit_lin, label='Linear Decay', linestyle='--')
    plt.plot(xs, y_fit_pow, label=rf'$f(N_s) = {round(popt_pow[0],4)}\cdot N_s^' + '{-' + str(round(popt_pow[1],4)) + '}' + f'+{round(popt_pow[2],4)}$', linestyle='--')
    # plt.plot(x, y_fit_log, label='Logarithmic Decay', linestyle='--')
    #plt.plot(x, y_fit_inv, label='Inverse Decay', linestyle='--')

    # print(f"Power Law Decay: {r2_pow}")
    # print(f"Logarithmic Decay: {r2_log}")
    print(f"Power Law Decay: a = {popt_pow[0]}, b = {popt_pow[1]} c={popt_pow[2]}")
    # print(f"Logarithmic Decay: a = {popt_log[0]}, b = {popt_log[1]}")

    plt.xlabel(r"Number of random bases $N_s$")
    plt.ylabel(r'$d_n(S_N,V_N)$')
    #plt.title(f'Algorithm 1: N={10}, n={5}')
    main.show_plot(True)
