# Thesis source code

### main.py
contains the implementations of the four algorithms and more
### examplePlots.py
contains code used for making plots
### decayCurveFit.py
contains the fitting of a power law to the decay of Algorithm 1 in terms of the number of random basis samples
